## Some instuctions

- create python3 env
```
    virtualenv -p python3 env
```

- activate environment
```
    source env/bin/activate
```

- install dependencies
```
    pip install -r requirements.txt
```

- Download speech
```
    python3 -m datasets.davaa.download
```

- Segment all audios on silence.
```
    python3 -m audio.silence --audio_pattern "./datasets/davaa/audio/*.wav" --method=pydub
```

- Generate numpy files which will be used in training.
```
    python3 -m datasets.generate_data ./datasets/davaa/alignment.json
```

- Download subtitles
```
    ./scripts/download-davaa-subtitles.sh
```

