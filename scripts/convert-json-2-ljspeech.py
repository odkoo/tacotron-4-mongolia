import json, os, csv

json_path = "../datasets/davaa/recognition.json"

def csv_writer(data, path):
    with open(path, "w", newline='') as csv_file:        
        writer = csv.writer(csv_file, delimiter='|')
        for line in data:
            writer.writerow(line)

with open(json_path, encoding='utf-8') as data_file:
    data = json.loads(data_file.read())
    arr = []
    for key, val in data.items():
        t = os.path.splitext(os.path.basename(key))[0].split(".")
        name = t[0]+"-"+t[1]
        #print(name)
        arr.append([name, val, val])
    csv_writer(arr, "./metadata.csv")

